package com.cs.micro.demo.quartz2.task;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

/**
 * @author wangjiahao
 * @version 1.0
 * @className DemoTask
 * @since 2019-03-18 11:25
 */
@Component
public class DemoTask2 implements Job {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("#########demoTask2 run#############");
    }
}
